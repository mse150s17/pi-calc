#test
#Our Project 1 starter program for calculating pi
import numpy
import matplotlib.pyplot as plt

#We want to calculate pi using the Monte Carlo Method
#Throw darts at a square dartboard

#Count darts that go in circle vs total number of darts = pi/4

#Need ability to throw darts at a square
def throw_dart():
    x = numpy.random.random() #each of these should be between 0 and 1
    y = numpy.random.random()
    return x,y
                
#Need ability to determine if the dart is in the circle
def in_circle(point): 
    d = (point[0]**2 + point[1]**2)**(0.5)
    if d > 1.0:
        return False
    return True

##Call my functions down here
#Runs 1000 trials to establish a large sample of answers
inside = 0
answers = []
trials = 1000
for i in range(trials):
	if in_circle(throw_dart()):
		inside += 1
		answers.append(inside)
#inside / trials gives the number of darts landing inside the 	
print(4*inside/trials)
print((3.1415926535-4*numpy.array(answers)/trials)/3.1415926535)
plt.plot((3.1415926535-numpy.array(answers)/trials)/3.1415926535)
